
## This repository is being moved to [https://github.com/Bildungsportal/next-exam](https://github.com/Bildungsportal/next-exam)
It will remain publicly accessible as an archive for reference.



# next-exam
A next-generation digital examination environment for the secure administration of tests, school assignments, and final exams.

# technologies used
* electron
* vite
* node
* node-express
* vue
* bootstrap


## download latest builds
https://next-exam.at


![screenshot](/info/screenshot.png)




